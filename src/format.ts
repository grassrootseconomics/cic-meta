interface JSONSerializable {
	toJSON(): string
}

export { JSONSerializable };
